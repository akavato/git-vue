module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    "brace-style": ["error", "stroustrup", {"allowSingleLine": true}],
        "indent": ["error", 4],
        "no-undef": "warn",
        "no-unused-vars": ["warn", {
            "vars": "all",
            "args": "none",
            "ignoreRestSiblings": true
        }],
        "no-useless-escape":"off",
        "padded-blocks": ["warn", "never"],
        "semi": ["error", "always"],
        "spaced-comment": ["warn", "always"],
        "space-before-function-paren": ["warn", "never"]
  }
}
