import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import InfiniteLoading from 'vue-infinite-loading';

Vue.config.productionTip = false;
Vue.use(InfiniteLoading, {
    slots: {
        noMore: {
            render: h => h('div')
        }
    }
});

new Vue({
    vuetify,
    render: h => h(App)
}).$mount('#app');
