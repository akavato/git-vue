import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import { mdiMagnify } from '@mdi/js';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdiSvg',
        values: {
            search: mdiMagnify
        }
    },
    rtl: false,
    theme: {
        disable: true
    }
});
